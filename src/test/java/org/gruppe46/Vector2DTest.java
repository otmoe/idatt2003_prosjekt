package org.gruppe46;

import org.gruppe46.data.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Vector2DTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void getX0pos() {
        Vector2D v = new Vector2D(21.2, 13.2);
        assertEquals(21.2, v.getX0());
    }

    @Test
    void getX0neg() {
        Vector2D v = new Vector2D(21.2, 13.2);
        assertNotEquals(13.2, v.getX0());
    }

    @Test
    void getX1pos() {
        Vector2D v = new Vector2D(21.2, 13.2);
        assertEquals(13.2, v.getX1());
    }

    @Test
    void getX1neg() {
        Vector2D v = new Vector2D(21.2, 13.2);
        assertNotEquals(21.2, v.getX1());
    }

    @Test
    void addpos() {
        Vector2D v = new Vector2D(21.2, 13.2);
        Vector2D u = new Vector2D(23.6, 17.3);
        assertEquals(44.8, v.add(u).getX0());
        assertEquals(30.5, v.add(u).getX1());
        assertEquals(44.8, u.add(v).getX0());
        assertEquals(30.5, u.add(v).getX1());
    }

    @Test
    void addneg() {
        Vector2D v = new Vector2D(21.2, 13.2);
        Vector2D u = new Vector2D(23.6, 17.3);
        assertNotEquals(30.5, v.add(u).getX0());
        assertNotEquals(44.8, v.add(u).getX1());
        assertNotEquals(30.5, u.add(v).getX0());
        assertNotEquals(44.8, u.add(v).getX1());
    }

    @Test
    void subtractpos() {
        Vector2D v = new Vector2D(21.2, 13.2);
        Vector2D u = new Vector2D(23.6, 17.3);
        assertEquals(-2.4, v.subtract(u).getX0(), 0.0001);
        assertEquals(-4.1, v.subtract(u).getX1(), 0.0001);
        assertEquals(2.4, u.subtract(v).getX0(), 0.0001);
        assertEquals(4.1, u.subtract(v).getX1(), 0.0001);
    }

    @Test
    void subtractneg() {
        Vector2D v = new Vector2D(21.2, 13.2);
        Vector2D u = new Vector2D(23.6, 17.3);
        assertNotEquals(2.4, v.subtract(u).getX0(), 0.0001);
        assertNotEquals(4.1, v.subtract(u).getX1(), 0.0001);
        assertNotEquals(-2.4, u.subtract(v).getX0(), 0.0001);
        assertNotEquals(-4.1, u.subtract(v).getX1(), 0.0001);
    }
}