package org.gruppe46;

import org.gruppe46.data.Matrix2x2;
import org.gruppe46.data.Vector2D;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Nested;

import static org.junit.jupiter.api.Assertions.*;

class Matrix2x2Test {

  @Nested
  @DisplayName("Positive tests")
  class PositiveTests {

    @Test
    void constructorTest() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);

      assertEquals(1, matrix.getA00());
      assertEquals(2, matrix.getA01());
      assertEquals(3, matrix.getA10());
      assertEquals(4, matrix.getA11());
    }

    @Test
    void multiplyTest() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
      Vector2D vector = new Vector2D(1, 2);
      Vector2D result = matrix.multiply(vector);

      assertEquals(5, result.getX0());
      assertEquals(11, result.getX1());
    }

    @Test
    void getA00Test() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);

      assertEquals(1, matrix.getA00());
    }

    @Test
    void getA01Test() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);

      assertEquals(2, matrix.getA01());
    }

    @Test
    void getA10Test() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);

      assertEquals(3, matrix.getA10());
    }

    @Test
    void getA11Test() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);

      assertEquals(4, matrix.getA11());
    }

    @Test
    void identityMatrixTest() {
      Matrix2x2 identityMatrix = new Matrix2x2(1, 0, 0, 1);
      Vector2D vector = new Vector2D(1, 2);
      Vector2D result = identityMatrix.multiply(vector);

      assertEquals(vector.getX0(), result.getX0());
      assertEquals(vector.getX1(), result.getX1());
    }

    @Test
    void zeroMatrixTest() {
      Matrix2x2 zeroMatrix = new Matrix2x2(0, 0, 0, 0);
      Vector2D vector = new Vector2D(1, 2);
      Vector2D result = zeroMatrix.multiply(vector);

      assertEquals(0, result.getX0());
      assertEquals(0, result.getX1());
    }

    @Test
    void boundaryTest() {
      Matrix2x2 matrix = new Matrix2x2(Double.MAX_VALUE, Double.MAX_VALUE, Double.MIN_VALUE, Double.MIN_VALUE);
      Vector2D vector = new Vector2D(1, 2);
      Vector2D result = matrix.multiply(vector);

      assertNotEquals(Double.MAX_VALUE + Double.MIN_VALUE, result.getX0());
      assertNotEquals(Double.MAX_VALUE + Double.MIN_VALUE, result.getX1());
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class NegativeTests {
  }
}