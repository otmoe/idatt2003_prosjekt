package org.gruppe46;

import org.gruppe46.data.Complex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ComplexTest {

    @BeforeEach
    void setUp() {

    }

    @Test
    void testSqrt() {
        Complex complex = new Complex(-5, -12);
        Complex result = complex.sqrt();
        assertEquals(2, result.getX0());
        assertEquals(-3, result.getX1());
    }

    @Test
    void testSign() {
        Complex complex = new Complex(-2, 5);
        assertEquals(-1, complex.sign(complex.getX0()));
        assertEquals(1, complex.sign(complex.getX1()));
    }

    @Test
    void testSubtract() {
        Complex complex1 = new Complex(2,5);
        Complex complex2 = new Complex(-1,1);
        Complex result = complex1.subtract(complex2);
        assertEquals(3, result.getX0());
        assertEquals(4, result.getX1());
    }

    @Test
    void testMultiply() {
        Complex complex = new Complex(-2, 5);
        Complex result = complex.multiply(3);
        assertEquals(-6, result.getX0());
        assertEquals(15, result.getX1());
    }
}