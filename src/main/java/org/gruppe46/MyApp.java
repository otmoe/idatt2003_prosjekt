package org.gruppe46;


import org.gruppe46.view.MainWindow;

/**
 * Class to start the application.
 */
public class MyApp {

  /**
   * Main method for the application.
   *
   * @param args Arguments for the application.
   * @throws Exception If an error occurs.
   */
  public static void main(String[] args) throws Exception {
    MainWindow.main(args);
  }
}