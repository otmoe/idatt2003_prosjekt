package org.gruppe46.userinterface;

import org.gruppe46.data.ChaosGame;
import org.gruppe46.data.ChaosGameDescription;
import org.gruppe46.filehandler.ChaosGameFileHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Simple Command Line Interface.
 */
public class CLI {

  /**
   * Choices for menu.
   *
   * @return The manu chosen.
   */
  public int menuChoice() {
    int choice = 0;
    System.out.println("---- Chaos Game CLI ----");
    System.out.println("1. Read from file");
    System.out.println("2. Write to file");
    System.out.println("3. Run iterations");
    System.out.println("4. Print ASCII");
    System.out.println("9. Exit");
    Scanner sc = new Scanner(System.in);
    if (sc.hasNextInt()) {
      choice = sc.nextInt();
    } else {
      System.out.println("Enter a number, not text");
    }
    return choice;
  }

  /**
   * Start the main CLI application.
   */
  public void startCLI() {
    final int READ_FILE = 1;
    final int WRITE_FILE = 2;
    final int RUN_GAME = 3;
    final int PRINT_GAME = 4;
    final int EXIT = 9;

    Scanner sc = new Scanner(System.in);
    ChaosGame game = null;

    boolean finished = false;
    while (!finished) {
      int choice = this.menuChoice();
      switch (choice) {
        case READ_FILE:
          File[] files = new File("src/main/resources").listFiles();
          if (files != null) {
            for (File file : files) {
              if (file.isFile()) {
                System.out.println(file.getName());
              }
            }
          }
          System.out.println("Enter a filename:");
          String filename = sc.nextLine();
          if (files != null) {
            for (File file : files) {
              if (file.isFile() && filename.equals(file.getName())) {
                try {
                  ChaosGameDescription chaosGameDescription =
                          new ChaosGameFileHandler().readFromFile(
                          "src/main/resources/" + filename);
                  game = new ChaosGame(chaosGameDescription, 32, 32);
                  System.out.println("File read successfully");
                  break;
                } catch (FileNotFoundException e) {
                  System.out.println("Could not read from file");
                }
              }
            }
          }
          break;
        case WRITE_FILE:
          if (game == null) {
            System.out.println("Read a file before saving to file");
            break;
          }
          File[] files2 = new File("src/main/resources").listFiles();
          if (files2 != null) {
            for (File file : files2) {
              if (file.isFile()) {
                System.out.println(file.getName());
              }
            }
          }
          String newFilename = "";
          int i = 0;
          while (i != 1) {
            i = 1;
            System.out.println("name of new file:");
            newFilename = sc.nextLine();
            newFilename += ".txt";
            if (files2 != null) {
              for (File file : files2) {
                if (file.isFile() && newFilename.equals(file.getName())) {
                  i = 0;
                  System.out.println("name already exists, please chose a new filename:");
                }
              }
            }
          }
          new ChaosGameFileHandler().writeToFile(
                  game.getDescription(),
                  "src/main/resources/" + newFilename);
          break;
        case RUN_GAME:
          if (game == null) {
            System.out.println("Read a file before running the game");
            break;
          }
          System.out.println("How many iterations?");
          if (sc.hasNextInt()) {
            game.runSteps(sc.nextInt());
            sc.nextLine();
          } else {
            System.out.println("You must enter a number, not text");
          }
          break;
        case PRINT_GAME:
          if (game == null) {
            System.out.println("Read a file before running the game");
            break;
          }
          int[][] canvasArray = game.getCanvas().getCanvasArray();
          for (int[] row : canvasArray) {
            for (int pixel : row) {
              if (pixel == 0) {
                System.out.print(" ");
              } else {
                System.out.print("X");
              }
            }
            System.out.println();
          }
          break;
        case EXIT:
          System.out.println("Shutting down.....");
          sc.close();
          finished = true;
          break;
        default:
          System.out.println("Unrecognized menu selected");
          break;
      }
    }
  }

  public static void main(String[] args) {
    new CLI().startCLI();
  }
}
