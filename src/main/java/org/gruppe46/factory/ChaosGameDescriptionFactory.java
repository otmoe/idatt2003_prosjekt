package org.gruppe46.factory;

import org.gruppe46.data.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory class for creating different types of ChaosGameDescription objects.
 */
public class ChaosGameDescriptionFactory {

  /**
   * Creates a ChaosGameDescription for a Sierpinski Triangle.
   *
   * @return A ChaosGameDescription for a Sierpinski Triangle.
   */
  public static ChaosGameDescription createSierpinskiTriangle() {
    List<Transform2D> transforms = new ArrayList<>();
    Transform2D transform1 =
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0));
    Transform2D transform2 =
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5));
    Transform2D transform3 =
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0));
    transforms.add(transform1);
    transforms.add(transform2);
    transforms.add(transform3);
    Vector2D minCoords = new Vector2D(0, 0);
    Vector2D maxCoords = new Vector2D(1, 1);
    return new ChaosGameDescription(transforms, minCoords, maxCoords, "affine2d");
  }

  /**
   * Creates a ChaosGameDescription for a Barnsley Fern.
   *
   * @return A ChaosGameDescription for a Barnsley Fern.
   */
  public static ChaosGameDescription createBarnsleyFern() {
    List<Transform2D> transforms = new ArrayList<>();
    Transform2D transform1 =
            new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0));
    Transform2D transform2 =
            new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85), new Vector2D(0, 1.6));
    Transform2D transform3 =
            new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22), new Vector2D(0, 1.6));
    Transform2D transform4 =
            new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24), new Vector2D(0, 0.44));
    transforms.add(transform1);
    transforms.add(transform2);
    transforms.add(transform3);
    transforms.add(transform4);
    Vector2D minCoords = new Vector2D(-3, 0);
    Vector2D maxCoords = new Vector2D(3, 10);
    return new ChaosGameDescription(transforms, minCoords, maxCoords, "affine2d");
  }

  /**
   * Creates a ChaosGameDescription for a Julia Set.
   *
   * @return A ChaosGameDescription for a Julia Set.
   */
  public static ChaosGameDescription createJuliaSet() {
    List<Transform2D> transforms = new ArrayList<>();
    Vector2D minCoords = new Vector2D(-1.6, -1);
    Vector2D maxCoords = new Vector2D(1.6, 1);

    Transform2D transform1 = new JuliaTransform(new Complex(-0.74543, 0.11301), 1);
    Transform2D transform2 = new JuliaTransform(new Complex(-0.74543, 0.11301), -1);

    transforms.add(transform1);
    transforms.add(transform2);
    return new ChaosGameDescription(transforms, minCoords, maxCoords, "julia");
  }
}
