package org.gruppe46.data;

import org.gruppe46.data.Matrix2x2;
import org.gruppe46.data.Transform2D;
import org.gruppe46.data.Vector2D;

/**
 * Class for simulating an affine transformation.
 */
public class AffineTransform2D extends Transform2D {

  private Matrix2x2 matrix;
  private Vector2D vector;

  /**
   * Constructor for AffineTransform2D.
   *
   * @param matrix The matrix for the affine transformation.
   * @param vector The vector for the affine transformation.
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * Method for affine transformation.
   *
   * @param point The input point.
   * @return The new Vector2D after the affine transformation.
   * @see Transform2D
   */
  @Override
  public Vector2D transform(Vector2D point) {
    double a00 = matrix.getA00();
    double a01 = matrix.getA01();
    double a10 = matrix.getA10();
    double a11 = matrix.getA11();

    double b0 = vector.getX0();
    double b1 = vector.getX1();

    double x0 = point.getX0();
    double x1 = point.getX1();

    double newX0 = a00 * x0 + a01 * x1 + b0;
    double newX1 = a10 * x0 + a11 * x1 + b1;

    return new Vector2D(newX0, newX1);
  }

  /**
   * Returns the matrix of the affine transformation.
   *
   * @return The matrix of the affine transformation.
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * Returns the vector of the affine transformation.
   *
   * @return The vector of the affine transformation.
   */
  public Vector2D getVector() {
    return vector;
  }

  /**
   * TooString method.
   *
   * @return The vectors as a string.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(this.getMatrix().toString());
    sb.append(", ");
    sb.append(this.getVector().toString());
    return sb.toString();
  }
}
