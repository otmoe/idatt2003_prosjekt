package org.gruppe46.data;

/**
 * Class that represent complex numbers.
 *
 * @author Ole-Thomas
 * @version v1.0
 */
public class Complex extends Vector2D {

  /**
   * Constructor for a Complex number.
   *
   * @param realPart      Value 0 of vector.
   * @param imaginaryPart Value 1 of vector.
   * @see Vector2D
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * Method that finds the square root of the complex number.
   *
   * @return The square root of the complex number.
   */
  public Complex sqrt() {
    double x = this.getX0();
    double y = this.getX1();

    double newX = Math.sqrt(0.5 * (Math.sqrt(x * x + y * y) + x));
    double newY = sign(y) * Math.sqrt(0.5 * (Math.sqrt(x * x + y * y) - x));

    return new Complex(newX, newY);
  }

  /**
   * Helper method to get sign of a value.
   *
   * @param x The value to get sign from.
   * @return Sign of input.
   */
  public double sign(double x) {
    double y;

    if (x >= 0) {
      y = 1;
    } else {
      y = -1;
    }
    return y;
  }

  /**
   * Method to add a complex number to the current complex number.
   *
   * @param other The complex number to add.
   * @return result Complex.
   */
  public Complex subtract(Complex other) {
    double x = this.getX0() - other.getX0();
    double y = this.getX1() - other.getX1();

    return new Complex(x, y);
  }

  /**
   * Method to multiply a complex number with a scalar.
   *
   * @param sign The scalar to multiply with.
   * @return result Complex.
   */
  public Complex multiply(double sign) {
    double x = this.getX0();
    double y = this.getX1();

    double newX = x * sign;
    double newY = y * sign;

    return new Complex(newX, newY);
  }

  /**
   * TooString method.
   *
   * @return The complex's elements as a string.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(this.getX0());
    sb.append(", ");
    sb.append(this.getX1());
    return sb.toString();
  }
}
