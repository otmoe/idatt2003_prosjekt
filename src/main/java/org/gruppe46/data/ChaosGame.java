package org.gruppe46.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class representing a Chaos Game, which is an Observable.
 */
public class ChaosGame implements Observable {
  private ChaosCanvas canvas;
  private ChaosGameDescription description;
  private Vector2D currentPoint;
  private Random random;
  private List<ChaosGameObserver> observers = new ArrayList<>();

  /**
   * Constructs a ChaosGame with the given parameters.
   *
   * @param description The description of the Chaos Game.
   * @param width       The width of the canvas.
   * @param hight       The height of the canvas.
   */
  public ChaosGame(ChaosGameDescription description, int width, int hight) {
    this.canvas = new ChaosCanvas(
            width,
            hight,
            description.getMinCoords(),
            description.getMaxCoords());
    this.description = description;
    this.currentPoint = new Vector2D(0, 0);
    this.random = new Random();
  }

  /**
   * Returns the canvas of the Chaos Game.
   *
   * @return The canvas of the Chaos Game.
   */
  public ChaosCanvas getCanvas() {
    return this.canvas;
  }

  /**
   * Returns the description of the Chaos Game.
   *
   * @return The description of the Chaos Game.
   */
  public ChaosGameDescription getDescription() {
    return this.description;
  }

  public void setCanvas(ChaosCanvas canvas) {
    this.canvas = canvas;
    notifyObservers();
  }

  public void setDescription(ChaosGameDescription description) {
    this.description = description;
    notifyObservers();
  }
  /**
   * Runs the Chaos Game for a given number of steps.
   *
   * @param steps The number of steps to run the game.
   */
  public void runSteps(int steps) {
    List<Transform2D> transforms = this.description.getTransforms();
    int length = transforms.size();
    int i = 0;
    while (i < steps) {
      int randInt = random.nextInt(length);
      Vector2D newPoint = transforms.get(randInt).transform(this.currentPoint);
      this.canvas.putPixel(newPoint);
      this.currentPoint = newPoint;
      i++;
    }
    notifyObservers();
  }

  /**
   * Clears the game.
   */
  public void clearGame() {
    this.getCanvas().clear();
    this.currentPoint = new Vector2D(0, 0);
    notifyObservers();
  }

  /**
   * Adds an observer to the Chaos Game.
   *
   * @param observer The observer to add.
   */
  @Override
  public void addObserver(ChaosGameObserver observer) {
    this.observers.add(observer);
  }

  /**
   * Removes an observer from the Chaos Game.
   *
   * @param observer The observer to remove.
   */
  @Override
  public void removeObserver(ChaosGameObserver observer) {
    this.observers.remove(observer);
  }

  /**
   * Notifies all observers of the Chaos Game.
   */
  @Override
  public void notifyObservers() {
    for (ChaosGameObserver observer : observers) {
      observer.update();
    }
  }
}
