package org.gruppe46.data;

import org.gruppe46.data.Transform2D;
import org.gruppe46.data.Vector2D;

import java.util.List;

/**
 * Class representing a description of a Chaos Game.
 */
public class ChaosGameDescription {

  /**
   * Vector of minimum coordinates.
   */
  private Vector2D minCoords;
  /**
   * Vector of maximum coordinates.
   */
  private Vector2D maxCoords;
  /**
   * List of all transforms.
   */
  private List<Transform2D> transforms;
  /**
   * Type of transform.
   */
  private String type;

  /**
   * Constructs a ChaosGameDescription with the given parameters.
   *
   * @param transforms The list of transformations.
   * @param minCoords  The minimum coordinates.
   * @param maxCoords  The maximum coordinates.
   * @param type       The type of the ChaosGame.
   */
  public ChaosGameDescription(
          List<Transform2D> transforms,
          Vector2D minCoords,
          Vector2D maxCoords,
          String type) {
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transforms = transforms;
    this.type = type;
  }

  /**
   * Returns the list of transformations.
   *
   * @return The list of transformations.
   */
  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Returns the minimum coordinates.
   *
   * @return The minimum coordinates.
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Returns the maximum coordinates.
   *
   * @return The maximum coordinates.
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Returns the type of the Chaos Game.
   *
   * @return The type of the Chaos Game.
   */
  public String getType() {
    return type;
  }

  /**
   * Sets the minimum coordinates.
   *
   * @param newMinCoords The new minimum coordinates.
   */
  public void setMinCoords(Vector2D newMinCoords) {
    this.minCoords = newMinCoords;
  }

  /**
   * Sets the maximum coordinates.
   *
   * @param newMaxCoords The new maximum coordinates.
   */
  public void setMaxCoords(Vector2D newMaxCoords) {
    this.maxCoords = newMaxCoords;
  }

  /**
   * Sets the list of transformations.
   *
   * @param transforms The new list of transformations.
   */
  public void setTransforms(List<Transform2D> transforms) {
    this.transforms = transforms;
  }

  /**
   * Sets the type of the Chaos Game.
   *
   * @param type The new type of the Chaos Game.
   */
  public void setType(String type) {
    this.type = type;
  }

  /**
   * TooString method.
   *
   * @return The representation of elements as a string.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(getType());
    sb.append("\n");
    sb.append(getMinCoords());
    sb.append("\n");
    sb.append(getMaxCoords());
    sb.append("\n");
    for (Transform2D transform : getTransforms()) {
      sb.append(transform);
      sb.append("\n");
    }
    return sb.toString();
  }

}
