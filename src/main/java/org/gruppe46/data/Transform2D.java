package org.gruppe46.data;

import org.gruppe46.data.Vector2D;

/**
 * Interface for a Transform2D.
 *
 * @author Ole-Thomas
 * @version v1.0
 */
public abstract class Transform2D {

  /**
   * Transform2D method.
   *
   * @param point The input point.
   * @return The Vector2D created by the transformation.
   */
  public abstract Vector2D transform(Vector2D point);
}
