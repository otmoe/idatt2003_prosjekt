package org.gruppe46.data;

/**
 * Class for simulating a Julia transformation.
 *
 * @author Marcus Westum
 * @version v1.0
 */

public class JuliaTransform extends Transform2D {

  /**
   * The current point of the JuliaTransform.
   */
  private Complex point;
  /**
   * the sign for the Julia transformation.
   */
  private int sign;

  /**
   * Constructor for JuliaTransform.
   *
   * @param point The point for the Julia transformation.
   * @param sign  The sign for the Julia transformation.
   */
  public JuliaTransform(Complex point, int sign) {
    this.point = point;
    this.sign = sign;
  }

  /**
   * Method for Julia transformation.
   *
   * @param z The input point.
   * @return The new Vector2D after the Julia transformation.
   * @see Transform2D
   */
  @Override
  public Vector2D transform(Vector2D z) {
    Complex complexZ = new Complex(z.getX0(), z.getX1());

    Complex zMinusC = complexZ.subtract(point);

    Complex sqrtZMinusC = zMinusC.sqrt();

    Complex result = sqrtZMinusC.multiply(sign);

    return new Vector2D(result.getX0(), result.getX1());
  }

  /**
   * Getter for point.
   *
   * @return The current point.
   */
  public Complex getPoint() {
    return point;
  }

  /**
   * TooString method.
   *
   * @return The JuliaTransforms elements as a string.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(this.getPoint().toString());
    return sb.toString();
  }
}
