package org.gruppe46.data;

import org.gruppe46.data.ChaosGameObserver;

/**
 * Interface for the observable.
 */
public interface Observable {

  /**
   * Adds an observer to the list of observers.
   *
   * @param observer To be added.
   */
  void addObserver(ChaosGameObserver observer);

  /**
   * Removes an observer from the list of observers.
   *
   * @param observer To be removed.
   */
  void removeObserver(ChaosGameObserver observer);

  /**
   * Notifies all the observers in the list of observers.
   */
  void notifyObservers();
}
