package org.gruppe46.data;

/**
 * Class representing a 2x2 matrix.
 *
 * @author Marcus Westum
 * @version v1.0
 */
public class Matrix2x2 {

  /**
   * Elements of the matrix.
   */
  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;

  /**
   * Constructor creating a 2x2 matrix.
   *
   * @param a00 Element 00 of the matrix.
   * @param a01 Element 01 of the matrix.
   * @param a10 Element 10 of the matrix.
   * @param a11 Element 11 of the matrix.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Method for multiplying a 2x2 matrix with a 2D vector.
   *
   * @param vector The vector to multiply the matrix with.
   * @return Vector2D The result from the matrix-vector multiplication.
   * @see Vector2D
   */
  public Vector2D multiply(Vector2D vector) {
    double x0 = vector.getX0();
    double x1 = vector.getX1();

    double y0 = a00 * x0 + a01 * x1;
    double y1 = a10 * x0 + a11 * x1;

    return new Vector2D(y0, y1);

  }

  /**
   * Getters for elements in the matrix.
   *
   * @return The element in the matrix.
   */
  public double getA00() {
    return a00;
  }

  public double getA01() {
    return a01;
  }

  public double getA10() {
    return a10;
  }

  public double getA11() {
    return a11;
  }

  /**
   * TooString method.
   *
   * @return The matrices elements as a string.
   */
  @Override
  public String toString() {
    return this.getA00()
            + ", "
            + this.getA01()
            + ", "
            + this.getA10()
            + ", "
            + this.getA11();
  }
}
