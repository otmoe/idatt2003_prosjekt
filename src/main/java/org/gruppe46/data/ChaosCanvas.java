package org.gruppe46.data;

/**
 * Class representing the canvas of the chaos game.
 *
 * @author Ole-Thomas
 * @version v1.0
 */
public class ChaosCanvas {

  private int[][] canvas;
  private int width;
  private int height;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private final AffineTransform2D transformCoordsToIndices;

  /**
   * Constructor for the chaos canvas class.
   *
   * @param width     Width of the canvas.
   * @param height    Height of the canvas.
   * @param minCoords Min coords of the canvas.
   * @param maxCoords Max coords of the canvas.
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    this.canvas = new int[height][width];
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;

    this.transformCoordsToIndices = new AffineTransform2D(
            new Matrix2x2(
                    0,
                    (height - 1) / (minCoords.getX1() - maxCoords.getX1()),
                    (width - 1) / (maxCoords.getX0() - minCoords.getX0()),
                    0
            ),
            new Vector2D(
                    ((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
                    ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0())
            )
    );
  }

  /**
   * Getter for "pixel" at a point in the canvas.
   *
   * @param point The point to check the pixel.
   * @return The value of the pixel.
   */
  public int getPixel(Vector2D point) {
    Vector2D newPoint = transformCoordsToIndices.transform(point);

    if ((int) newPoint.getX0() <= height
                    && (int) newPoint.getX0() >= 0
                    && (int) newPoint.getX1() <= height
                    && (int) newPoint.getX1() >= 0) {
      return this.canvas[(int) newPoint.getX0()][(int) newPoint.getX1()];
    } else {
      return 0;
    }
  }

  /**
   * Setter for "pixel" at a point in the canvas.
   *
   * @param point The point to sett the pixel value to 1.
   */
  public void putPixel(Vector2D point) {
    Vector2D newPoint = transformCoordsToIndices.transform(point);
    if ((int) newPoint.getX0() <= height
            && (int) newPoint.getX0() >= 0
            && (int) newPoint.getX1() <= width
            && (int) newPoint.getX1() >= 0) {
      this.canvas[(int) newPoint.getX0()][(int) newPoint.getX1()] += 1;
    }
  }

  /**
   * Getter for the canvas array.
   *
   * @return The canvas array.
   */
  public int[][] getCanvasArray() {
    return this.canvas;
  }

  /**
   * Clear method to clear/reset the canvas.
   */
  public void clear() {
    this.canvas = new int[this.height][this.width];
  }
}
