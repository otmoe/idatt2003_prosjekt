package org.gruppe46.data;

/**
 * Vector2D class.
 *
 * @author Ole-Thomas
 * @version v1.0
 */
public class Vector2D {

  /**
   * Value 0 of vector.
   */
  private double x0;
  /**
   * Value 1 of vector.
   */
  private double x1;

  /**
   * Constructor for a Vector2D.
   *
   * @param x0 Value 0 of vector.
   * @param x1 Value 1 of vector.
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Getter for 0 element in vector.
   *
   * @return Value 0 of vector
   */
  public double getX0() {
    return x0;
  }

  /**
   * Getter for 1 element in vector.
   *
   * @return Value 1 of vector.
   */
  public double getX1() {
    return x1;
  }

  /**
   * Method to add a vector to the current vector.
   *
   * @param other the vector to add.
   * @return Result Vector2D.
   */
  public Vector2D add(Vector2D other) {
    double y0 = this.x0 + other.getX0();
    double y1 = this.x1 + other.getX1();

    return new Vector2D(y0, y1);
  }

  /**
   * Method to subtract a vector to the current vector.
   *
   * @param other The vector to subtract.
   * @return Result Vector2D.
   */
  public Vector2D subtract(Vector2D other) {
    double y0 = this.x0 - other.getX0();
    double y1 = this.x1 - other.getX1();
    return new Vector2D(y0, y1);
  }

  /**
   * TooString method.
   *
   * @return The vectors elements as a string.
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(this.getX0());
    sb.append(", ");
    sb.append(this.getX1());
    return sb.toString();
  }
}
