package org.gruppe46.data;

/**
 * Observer interface for the observer.
 */
public interface ChaosGameObserver {

  /**
   * Updates the observer.
   */
  public void update();
}
