package org.gruppe46.filehandler;

import org.gruppe46.data.*;

import java.io.*;
import java.util.*;

/**
 * Class handling file operations for the ChaosGame.
 */
public class ChaosGameFileHandler {

  /**
   * Reads a ChaosDescription from a file.
   *
   * @param path The path to the file.
   * @return The ChaosGameDescription read from the file.
   * @throws FileNotFoundException If the file does not exist
   */
  public ChaosGameDescription readFromFile(String path) throws FileNotFoundException {
    File file = new File(path);
    try (Scanner scanner = new Scanner(file)) {
      scanner.useDelimiter("\\s|,\\s*");
      scanner.useLocale(Locale.ENGLISH);
      String transformType = scanner.nextLine().trim().toLowerCase().split(" ")[0];

      Vector2D minCoords = new Vector2D(readDouble(scanner), readDouble(scanner));
      Vector2D maxCoords = new Vector2D(readDouble(scanner), readDouble(scanner));

      List<Transform2D> transform = parseTransformations(scanner, transformType);

      return new ChaosGameDescription(transform, minCoords, maxCoords, transformType);
    } catch (FileNotFoundException e) {
      throw new FileNotFoundException("File '" + path + "' not found.");
    } catch (InputMismatchException e) {
      throw new InputMismatchException("Invalid input in file '" + path + "'.");
    }
  }

  /**
   * Reads a double value from the Scanner.
   *
   * @param scanner The scanner to read from.
   * @return The double value read from the Scanner.
   */
  private double readDouble(Scanner scanner) {
    double value = scanner.nextDouble();
    if (scanner.hasNextLine() && !scanner.hasNextDouble()) {
      scanner.nextLine();
    }
    return value;
  }

  /**
   * Parses Affine2D transformations from the Scanner.
   *
   * @param scanner       The Scanner to parse from.
   * @param transformType The type of transformation to parse.
   * @return A list of Transform2D objects representing the transformations.
   */
  private List<Transform2D> parseTransformations(
          Scanner scanner,
          String transformType) {
    return switch (transformType) {
      case "affine2d" -> parseAffine2DTransformation(scanner);
      case "julia" -> parseJuliaTransformations(scanner);
      default -> throw new IllegalArgumentException(
              "Unknown transformation type: " + transformType);
    };
  }

  /**
   * Parses Affine2D transformations from a Scanner.
   *
   * @param scanner The Scanner to parse from.
   * @return A list of Transform2D objects representing the Affine2D transformations.
   */
  private List<Transform2D> parseAffine2DTransformation(Scanner scanner) {
    List<Transform2D> transform = new ArrayList<>();
    Matrix2x2 matrix;
    Vector2D vector;
    while (scanner.hasNextLine()) {
      matrix = new Matrix2x2(readDouble(scanner), readDouble(scanner),
              readDouble(scanner), readDouble(scanner));
      vector = new Vector2D(readDouble(scanner), readDouble(scanner));
      transform.add(new AffineTransform2D(matrix, vector));
    }
    return transform;
  }

  /**
   * Parses Julia transformations from a Scanner.
   *
   * @param scanner The Scanner to parse from.
   * @return A list of Transform2D objects representing the Julia transformation.
   */
  private List<Transform2D> parseJuliaTransformations(Scanner scanner) {
    List<Transform2D> transforms = new ArrayList<>();
    double realPart = readDouble(scanner);
    double imaginaryPart = readDouble(scanner);
    Complex complex = new Complex(realPart, imaginaryPart);
    transforms.add(new JuliaTransform(complex, 1));
    transforms.add(new JuliaTransform(complex, -1));
    return transforms;
  }

  /**
   * Writes a ChaosGameDescription to a file.
   *
   * @param chaosGameDescription The ChaosGameDescription to write.
   * @param path                 The path to the file.
   */
  public void writeToFile(ChaosGameDescription chaosGameDescription, String path) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
      writer.write(chaosGameDescription.toString());
    } catch (IOException e) {
      throw new IllegalArgumentException("Could not write to file '" + path + "'.");
    }
  }
}