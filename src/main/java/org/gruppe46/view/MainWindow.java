package org.gruppe46.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import org.gruppe46.data.*;
import org.gruppe46.filehandler.ChaosGameFileHandler;

/**
 * Main window of the application.
 */
public class MainWindow extends Application implements ChaosGameObserver {

  private static final int WIDTH = 550;
  private static final int HEIGHT = 550;

  private ChaosGame game = null;
  private WritableImage writableImage;

  /**
   * Method to lunch the application.
   *
   * @param args Arguments to the application
   */
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    ChaosGameFileHandler chaosGameFileHandler;
    try {

      writableImage = new WritableImage(WIDTH+50, HEIGHT+50);

      chaosGameFileHandler = new ChaosGameFileHandler();

      try {
        ChaosGameDescription chaosGameDescription = chaosGameFileHandler.readFromFile(
                "src/main/resources/barnsley-fern.txt");
        game = new ChaosGame(chaosGameDescription, WIDTH, HEIGHT);
        this.game.addObserver(this);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }

      ImageView imageView = new ImageView(writableImage);

      imageView.setRotate(90);
      imageView.setScaleY(-1);
      imageView.setFitHeight(HEIGHT);
      imageView.setFitWidth(WIDTH);

      BorderPane root = new BorderPane();
      root.setPadding(new Insets(10,10,10,10));
      root.setLeft(imageView);

      VBox rightBox = new VBox(8);
      rightBox.setPadding(new Insets(8, 8, 8, 0));

      //Buttons
      Button playButton = new Button("Play");
      Button resetButton = new Button("RESET");
      Button setValuesButton = new Button("Set values");
      Button saveButton = new Button("Save");

      //Input fields
      TextField stepInput = new TextField();
      TextField minCoord = new TextField();
      TextField maxCoord = new TextField();
      TextField konstC = new TextField();
      TextField matrixA = new TextField();
      TextField vectorB = new TextField();

      //Dropdown menu to change resolution
      final ComboBox resolutionMenu = new ComboBox<>();
      resolutionMenu.getItems().addAll(
              "800x600",
              "1024x768",
              "1280x1024",
              "1600x900",
              "1920x1080"
      );

      //Default value for resolution menu
      resolutionMenu.setValue("800x600");

      //Dropdown menu for saved fractals
      final ComboBox chaosMenu = new ComboBox<>();
      File[] files = new File("src/main/resources").listFiles();
      if (files != null) {
        for (File file : files) {
          if (file.isFile()) {
            chaosMenu.getItems().add(file.getName());
          }
        }
      }

      //Default value for saved fractals menu
      chaosMenu.setValue("bransley-fern.txt");

      //Labels
      Label resolutionMenuLabel = new Label("Resolution Menu:");
      Label chaosMenuLabel = new Label("Chaos Menu:");
      Label stepInputLabel = new Label("Number of steps:");
      Label minCoordLabel = new Label("Min Coords:");
      Label maxCoordLabel = new Label("Max Coords:");
      Label konstCLabel = new Label("Konst C (Julia):");
      Label matrixALabel = new Label("Matrix A (Affine):");
      Label vectorBLabel = new Label("Vector b (Affine):");

      HBox buttonBox1 = new HBox(8);

      buttonBox1.getChildren().addAll(playButton, resetButton);

      HBox buttonBox2 = new HBox(8);

      buttonBox2.getChildren().addAll(setValuesButton, saveButton);

      rightBox.getChildren().addAll(
              resolutionMenuLabel,
              resolutionMenu,
              chaosMenuLabel,
              chaosMenu,
              stepInputLabel,
              stepInput,
              buttonBox1,
              minCoordLabel,
              minCoord,
              maxCoordLabel,
              maxCoord,
              konstCLabel,
              konstC,
              matrixALabel,
              matrixA,
              vectorBLabel,
              vectorB,
              buttonBox2);
      root.setRight(rightBox);

      //Event handling for buttons

      //Event handling for play button
      playButton.setOnAction(event -> {
        try {
          int steps = Integer.parseInt(stepInput.getText());
          game.runSteps(steps);
        } catch (NumberFormatException e) {
          integerAlert("Number of steps");
        }
      });

      //Event handling for reset button
      resetButton.setOnAction(event -> {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Reset game?");
        alert.setContentText("Are you sure you want to reset the game?");

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
          game.clearGame();
        }
      });

      //Event handling for set values button
      setValuesButton.setOnAction(event -> {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Set new values?");
        alert.setContentText("""
                Are you sure you want to set new values?
                You can have multiple affine transforms by using a ':' between the matix/vector.
                Remember to have the same amounts of matrices and vectors.
                """);

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
          try {
            String[] newMinCoordsInput = minCoord.getText().split(",", 2);
            Vector2D newMinCoords = new Vector2D(
                    Double.parseDouble(newMinCoordsInput[0]),
                    Double.parseDouble(newMinCoordsInput[1]));

            String[] newMaxCoordsInput = maxCoord.getText().split(",", 2);
            Vector2D newMaxCoords = new Vector2D(
                    Double.parseDouble(newMaxCoordsInput[0]),
                    Double.parseDouble(newMaxCoordsInput[1]));

            if (game.getDescription().getType().equals("julia")) {
              String[] newKonstCInput = konstC.getText().split(",", 2);
              Complex newKonstC = new Complex(
                      Double.parseDouble(newKonstCInput[0]),
                      Double.parseDouble(newKonstCInput[1]));

              List<Transform2D> transform = new ArrayList<>();
              transform.add(new JuliaTransform(newKonstC, (int) Math.signum(newKonstC.getX1())));
              game.getDescription().setTransforms(transform);
            } else if (game.getDescription().getType().equals("affine2d")) {
              String[] matrices = matrixA.getText().split(":");
              String[] vectors = vectorB.getText().split(":");

              List<Transform2D> transform = new ArrayList<>();

              if (matrices.length == vectors.length) {
                for (int i = 0; i < matrices.length; i++) {
                  String[] newMatrixAInput = matrices[i].split(",", 4);
                  Matrix2x2 newMatrixA = new Matrix2x2(
                          Double.parseDouble(newMatrixAInput[0]),
                          Double.parseDouble(newMatrixAInput[1]),
                          Double.parseDouble(newMatrixAInput[2]),
                          Double.parseDouble(newMatrixAInput[3]));

                  String[] newVecBInput = vectors[i].split(",", 2);
                  Vector2D newVecB = new Vector2D(
                          Double.parseDouble(newVecBInput[0]),
                          Double.parseDouble(newVecBInput[1]));

                  transform.add(new AffineTransform2D(newMatrixA, newVecB));
                }

                game.getDescription().setTransforms(transform);
              } else {
                Alert alert1 = new Alert(Alert.AlertType.ERROR);
                alert1.setTitle("Error");
                alert1.setHeaderText("Input error");
                alert1.setContentText("The number of vectors and matrices are not the same!");
                alert1.show();
                return;
              }
            } else {
              return;
            }

            game.getDescription().setMinCoords(newMinCoords);
            game.getDescription().setMaxCoords(newMaxCoords);
          } catch (NumberFormatException e) {
            vectorMatrixAlert();
          }
        }
      });

      //Event handling for save button
      saveButton.setOnAction(event -> {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Save values?");
        alert.setContentText("""
                Are you sure you want to save values?
                Remember to set  values before saving
                """);

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
          TextInputDialog dialog = new TextInputDialog("File name");
          dialog.setTitle("Text Input Dialog");
          dialog.setHeaderText("");
          dialog.setContentText("Please enter a new filename");

          Optional<String> result2 = dialog.showAndWait();
          if (result2.isPresent()) {
            String filename = result2.get() + ".txt";

            if (files != null) {
              for (File file : files) {
                if (file.isFile() && file.toString().equals(filename)) {
                  Alert alert3 = new Alert(Alert.AlertType.ERROR);
                  alert3.setTitle("Error");
                  alert3.setHeaderText("");
                  alert3.setContentText("""
                          There is already a file with that name.
                          """);
                  alert3.show();
                  return;
                }
              }
            }
            try {
              File file = new File("src/main/resources/" + filename);
              file.createNewFile();
            } catch (Exception e) {
              e.printStackTrace();
            }
            chaosGameFileHandler.writeToFile(game.getDescription(),
                    "src/main/resources/" + filename);
          }

        }
      });

      //Event handling for resolution menu
      resolutionMenu.setOnAction(event -> {
        String selectedResolution = (String) resolutionMenu.getSelectionModel().getSelectedItem();
        String[] dimensions = selectedResolution.split("x");
        primaryStage.setWidth(Integer.parseInt(dimensions[0]));
        primaryStage.setHeight(Integer.parseInt(dimensions[1]));
        imageView.setFitHeight(Integer.parseInt(dimensions[1]));
        imageView.setFitWidth(Integer.parseInt(dimensions[1]));
      });

      //Event handling for chaos menu
      chaosMenu.setOnAction(event -> {
        String selectedGame = (String) chaosMenu.getSelectionModel().getSelectedItem();
        try {
          ChaosGameDescription description = chaosGameFileHandler.readFromFile("src/main/resources/" + selectedGame);
          game.setDescription(description);
          game.setCanvas(new ChaosCanvas(WIDTH, HEIGHT, game.getDescription().getMinCoords(), game.getDescription().getMaxCoords()));
          game.clearGame();
        } catch (FileNotFoundException e) {
          throw new RuntimeException(e);
        }
      });

      Scene scene = new Scene(root, 800, 600);

      primaryStage.setScene(scene);
      primaryStage.setTitle("Chaos Game");
      primaryStage.setResizable(false);
      primaryStage.show();


    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  private void updateMainWindow() {
    updateCanvas(this.game, this.writableImage);
  }

  @Override
  public void update() {
    updateMainWindow();
  }

  private void vectorMatrixAlert() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("Input error");
    alert.setContentText("""
            Invalid input
            Please input a valid vector/matrix.
            NO SPACES
            Example Vector: 2.1,3.6 Matrix: 2.2,3.2,3.5,2.1
            Min/max coords are vectors!""");
    alert.show();
  }

  private void integerAlert(String name) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error");
    alert.setHeaderText("Input error");
    alert.setContentText("Invalid input of " + name + ".\nPlease input a valid fraction.");
    alert.show();
  }

  private void updateCanvas(ChaosGame game, WritableImage writableImage) {
    int[][] canvas = game.getCanvas().getCanvasArray();

    // Clear the writable image
    for (int i = 0; i < WIDTH; i++) {
      for (int j = 0; j < HEIGHT; j++) {
        writableImage.getPixelWriter().setColor(i, j, Color.WHITE);
      }
    }

    // Color the writable image canvas
    for (int i = 0; i < WIDTH; i++) {
      for (int j = 0; j < HEIGHT; j++) {
        if (canvas[i][j] > 7 ) {
          writableImage.getPixelWriter().setColor(i, j, Color.CRIMSON);
        } else if (canvas[i][j] > 5 ) {
          writableImage.getPixelWriter().setColor(i, j, Color.GREEN);
        } else if (canvas[i][j] > 3 ) {
          writableImage.getPixelWriter().setColor(i, j, Color.BLUE);
        } else if (canvas[i][j] == 1 ) {
          writableImage.getPixelWriter().setColor(i, j, Color.BLACK);
        }
      }
    }
  }
}

